* [Containers](#containers)
  - [DecayLabel](#decaylabel)
* [Driver](#driver)
* [DecayLabel](#decay-label)
* [Example](#example)

## Containers

### Particle decay containers
| Class name                                 | Stored info                                        |
| ------------------------------------------ | -------------------------------------------------- |  
| `Decay_boson`                              | 4-vector/pdgId of W/Z's children particles and the decay label  |        
| `Decay_H` (derived from `Decay_boson`)     | 4-vector/pdgId of Higgs's (grand)children particles and the decay label  |        
| `Decay_tau`                                | 4-vector of visible tau, tau neutrino, decay label |

#### DecayLabel
`decayLabel` is an integer bit describing the decay mode.  
The definition can be found in `Decay_XXX::finalize` or `Decay_XXX::getDecayString` in the class implementation e.g.
[Tau](https://gitlab.cern.ch/shion/TruthDecayContainer/blob/master/Root/Decay_tau.cxx#L51-66) / 
[W,Z](https://gitlab.cern.ch/shion/TruthDecayContainer/blob/master/Root/Decay_boson.cxx#L129-159) /
[Higgs](https://gitlab.cern.ch/shion/TruthDecayContainer/blob/master/Root/Decay_H.cxx#L111) /
[EWKino](https://gitlab.cern.ch/shion/TruthDecayContainer/blob/master/Root/TruthEvent_XX.cxx#L108)

Executive summary can be found as following:  

**Tau**  
0: 1-prong  
1: 1-prong + 1-neutral  
2: 1-prong + n-neutral (n>=2)  
3: 3-prong  
4: 3-prong + n-neutral (n>=1)  
5: 2,4,5-prong  
6: 0,6-prong  
8: leptonic   

**W-boson**  
0: qq  
1: enu/munu  
2: taunu (leptau)  
3: taunu (hadtau)  

**Z-boson**  
0: qq   
1: ee/mumu  
2: leptau leptau  
3: leptau hadtau  
4: hadtau hadtau  
5: nunu  

**Higgs**  
0: yy  
1: bb  
2: tautau  
3: WW  
4: ZZ  
5: nunu  
6: uu/dd/ss    
7: cc
8: gg  

**EWKino**  
0: No decay (already N1)  
1: Decay via W  
2: Decay via Z  
3: Decay via H  


### Event containers
| Class name            | Stored info                                                               |
| --------------------- | ------------------------------------------------------------------------- |  
| `TruthEvent_Vjets`    | W/Z-decay and the associated jets                                         |
| `TruthEvent_VV`       | Decay of general VV                                                       |
| `TruthEvent_TT`       | Decay of TT                                                               |
| `TruthEvent_XX`       | Decay of pair produced electro-weak gaugino (only direct decay to chi10)  |
| `TruthEvent_GG`       | Decay of pair produced gluino (RPC, upto 1-step, no sleptons mediated)    |

## Driver
`DecayHandler` is the main processor that actually does the classification of decay chain.
Relevant functions are:
- `GetXXXDecay`: fill decay information of XXX into `Decay_XXX` class.  
- `GetDecayChain_YYY`: fill the whole decay chain of YYY into `TruthEvent_YYY` class.

## DSID-to-ProcessName mapping
Different truth retrival function should be called for different physics processes.  
`TruthDecayContainer/ProcClassifier.h` provides the map between DSID and the type of physics processes so that proper truth retriver function can be called in each job.

# Example
Minimal usage example can be found in `util/TruthDecayContainerTester.cxx`.  

To test, compile everything and execute:
<pre>
TruthDecayContainerTester [path-to-a-DAOD-file]
</pre>