#ifndef TRUTHDECAYUTILS_H
#define TRUTHDECAYUTILS_H

#include "TLorentzVector.h"
#include "TString.h"
#include "TVector3.h"

#include <iostream>
#include <iomanip>
#include <vector>


typedef TLorentzVector tlv;

//////////////////////////////////////////////////
//
// Easy utilities
//
/////////////////////////////////////////////////


namespace TruthDecayUtils{

 ///////////////////////////////////////
  // Swap the content of a and b.
 template <class T>
   inline void swap(T &a, T &b){
   T temp = a;
   a = b;  b = temp;  return;
 }

 ///////////////////////////////////////
 // DeltaR of two objects
 inline double GetDR(const double &eta1, const double &eta2, const double &phi1, const double &phi2){
   return hypot( eta1-eta2, acos(cos(phi1-phi2))  );   
 }
 inline double GetDR(const tlv &a, const tlv &b){
   return hypot(  a.Eta()-b.Eta(), acos(cos(a.Phi()-b.Phi())) );   
 }
 inline double GetDR_y(const tlv &a, const tlv &b){   
   return hypot(  a.Rapidity()-b.Rapidity(), acos(cos(a.Phi()-b.Phi())) );   
 }


 ///////////////////////////////////////
 // TLorentzVector of child particle in the parent rest frame
 inline  tlv boostBack_parent_RF(const tlv &child_lab, const tlv &parent_lab){
   tlv out = child_lab;
   if(parent_lab.Beta()<1) out.Boost(-parent_lab.BoostVector());  
   return out;
 }

 // Calulate the helicity angle 
 inline double costh_decay(const tlv &child_lab, const tlv &parent_lab){
   if(parent_lab.Beta()>=1) return -9; 
   tlv child_parent = boostBack_parent_RF(child_lab, parent_lab);
   return child_parent.Vect().Unit() * parent_lab.Vect().Unit();
 }

 ///////////////////////////////////////
 // Print 4-momentum. str: particle's name (optional)
 inline void print4mom(const tlv &a, TString name=""){
   std::cout << std::setw(4) << name << ": " 
	     << "pt " << setiosflags(std::ios::fixed) << std::setw(10) << a.Pt()
	     << "  px " << setiosflags(std::ios::fixed) << std::setw(10) << a.Px()
	     << "  py " << setiosflags(std::ios::fixed) << std::setw(10) << a.Py()
	     << "  pz " << setiosflags(std::ios::fixed) << std::setw(10) << a.Pz()
	     << "  cos " << setiosflags(std::ios::fixed) << std::setw(10) << a.CosTheta()
	     << "  eta " << setiosflags(std::ios::fixed) << std::setw(10) << a.Eta()
	     << "  phi " << setiosflags(std::ios::fixed) << std::setw(10) << a.Phi()
	     << "  E " << setiosflags(std::ios::fixed) << std::setw(10) << a.E()
	     << "  P " << setiosflags(std::ios::fixed) << std::setw(10) << a.P()    
	     << "  M " << setiosflags(std::ios::fixed) << std::setw(10) << a.M()
	     << "  Beta " << setiosflags(std::ios::fixed) << std::setw(10) << a.Beta()
	     << std::endl;
 }
 ///////////////////////////////////////




}
#endif
