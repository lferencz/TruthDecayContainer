#ifndef TRUTHEVENT_VVV_h
#define TRUTHEVENT_VVV_h

#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TObject.h"

#include "TruthDecayContainer/Decay_boson.h"
#include "TruthDecayContainer/Decay_H.h"

typedef TLorentzVector tlv;


class TruthEvent_VVV : public TObject {

 public:

  TruthEvent_VVV();

  ~TruthEvent_VVV();

  TruthEvent_VVV(const TruthEvent_VVV &rhs);

  virtual void set_evt(const Decay_boson &in_B1, const Decay_boson &in_B2, const Decay_boson &in_B3);
        
  virtual void finalize();
  
  virtual void print();

  virtual void clear();

  ClassDef(TruthEvent_VVV,1);

  // tlv 
  Decay_boson B1, B2, B3;
  Decay_H     H;
  TLorentzVector pMis;

  // decay label
  int idx_H, idx_nonH;

 private:  


};







#endif
