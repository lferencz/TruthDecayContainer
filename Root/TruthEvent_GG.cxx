#include "TruthDecayContainer/TruthEvent_GG.h"

#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TruthDecayContainer/TruthDecayUtils.h"

typedef TLorentzVector tlv;


TruthEvent_GG::TruthEvent_GG(){    clear();  }

TruthEvent_GG::~TruthEvent_GG(){}

TruthEvent_GG::TruthEvent_GG(const TruthEvent_GG &rhs) :
  TruthEvent_XX(rhs),
  pgo1(rhs.pgo1),
  pgo2(rhs.pgo2),
  pq11(rhs.pq11),
  pq12(rhs.pq12),
  pq21(rhs.pq21),
  pq22(rhs.pq22),
  decayLabel_q1(rhs.decayLabel_q1),
  decayLabel_q2(rhs.decayLabel_q2),
  q11_pdg(rhs.q11_pdg),
  q12_pdg(rhs.q12_pdg),
  q21_pdg(rhs.q21_pdg),
  q22_pdg(rhs.q22_pdg)
{}



///////////////////////////////////////////////////
void TruthEvent_GG::set_evt_GG(const tlv &in_pq11, const tlv &in_pq12, const int &in_q11_pdg, const int &in_q12_pdg, 
			       const tlv &in_pq21, const tlv &in_pq22, const int &in_q21_pdg, const int &in_q22_pdg, 	
			       const int &in_chi1_pdg, const int &in_chi2_pdg,
			       const tlv &in_pN1A, const tlv &in_pN1B,
			       const Decay_boson &in_B1, const Decay_boson &in_B2){

  TruthEvent_XX::set_evt_XX(in_chi1_pdg, in_chi2_pdg, in_pN1A, in_pN1B, in_B1, in_B2);

  pq11 = in_pq11;
  pq12 = in_pq12;
  q11_pdg = in_q11_pdg;
  q12_pdg = in_q12_pdg;

  pq21 = in_pq21;
  pq22 = in_pq22;
  q21_pdg = in_q21_pdg;
  q22_pdg = in_q22_pdg;
  
  finalize();
}

///////////////////////////////////////////////////
void TruthEvent_GG::finalize(){

  TruthEvent_XX::finalize();
  prodLabel = -1;

  // input: 
  //  pgo1,pgo2
  //  q11, q12, q21, q22, N1A, N1B (tlv, pdg)
  //  chi1, chi2, B1, B2

  pgo1 = pq11 + pq12 + pN1A + B1.P4;
  pgo2 = pq21 + pq22 + pN1A + B2.P4;
  pMis = pN1A+pN1A+B1.pMis+B2.pMis;
  
  decayLabel_q1=getQLabel(q11_pdg,q12_pdg);
  decayLabel_q2=getQLabel(q21_pdg,q22_pdg);

  return;
}

///////////////////////////////////////////////////
int TruthEvent_GG::getQLabel(const int &pdg1, const int &pdg2){

  // run 0-2
  // make sure always pdg1>0, pdg2<0
  if(    (pdg1==1 && pdg2==-1) 
	 || (pdg1==2 && pdg2==-2) 
	 || (pdg1==2 && pdg2==-1) 
	 || (pdg1==1 && pdg2==-2)
  
	 || (pdg1==3 && pdg2==-3) 
	 || (pdg1==4 && pdg2==-4) 
	 || (pdg1==4 && pdg2==-3) 
	 || (pdg1==3 && pdg2==-4)  )   return 0;                         // qqN1A(4), qq'C1(2), q'qC1(2), qqN2(4)  (): multiplicity

  else if( (pdg1==6 && pdg2==-5) || (pdg1==5 && pdg2==-6)  )   return 1; // bt+C1, tb+C1
  else if( pdg1==5 && pdg2==-5)             return 1;                    // direct(bb+N1A), bb+N2
  else if( pdg1==6 && pdg2==-6)             return 2;                    // direct(tt+N1A), tt+N2


  else return -1;  

}

                                                                       
///////////////////////////////////////////////////
void TruthEvent_GG::swap12(){
  TruthEvent_XX::swap12();
  TruthDecayUtils::swap(pgo1, pgo2);
  TruthDecayUtils::swap(pq11, pq12);
  TruthDecayUtils::swap(pq21, pq22);
  TruthDecayUtils::swap(q11_pdg, q21_pdg);
  TruthDecayUtils::swap(q21_pdg, q21_pdg);
  TruthDecayUtils::swap(decayLabel_q1, decayLabel_q2);
  return;                                                              
}                    

///////////////////////////////////////////////////
void TruthEvent_GG::check_swap(){                                      
                                                                       
  return;                                                              
}

////////////////////////////////////////////////////
void TruthEvent_GG::print(){

  std::cout << "TruthEvent_GG::print() **************************************" << std::endl;

  TruthEvent_XX::print();

  TruthDecayUtils::print4mom(pgo1, "pgo1");
  TruthDecayUtils::print4mom(pq11, "pq11");
  TruthDecayUtils::print4mom(pq12,  "pq12");
  TruthDecayUtils::print4mom(pgo2, "pgo2");
  TruthDecayUtils::print4mom(pq21, "pq21");
  TruthDecayUtils::print4mom(pq22,  "pq22");
  std::cout << "         ************************************************" << std::endl;
  std::cout << "  q11_pdg: " <<  q11_pdg << std::endl;
  std::cout << "  q12_pdg: " <<  q12_pdg << std::endl;
  std::cout << "  q21_pdg: " <<  q21_pdg << std::endl;
  std::cout << "  q22_pdg: " <<  q22_pdg << std::endl;
  std::cout << "********************************************************" << std::endl;

  std::cout << "decayLabel_q1: "<< decayLabel_q1 << std::endl;
  std::cout << "decayLabel_q2: "<< decayLabel_q2 << std::endl;

  return;
}

////////////////////////////////////////////////////
void TruthEvent_GG::clear(){

  TruthEvent_XX::clear();

  // tlv
  pgo1.SetXYZM(0,0,0,0); pgo2.SetXYZM(0,0,0,0);  
  pq11.SetXYZM(0,0,0,0); pq12.SetXYZM(0,0,0,0);  
  pq21.SetXYZM(0,0,0,0); pq22.SetXYZM(0,0,0,0);  

  // labels
  decayLabel_q1=-1;   
  decayLabel_q2=-1;   
  
  // pdg
  q11_pdg=0; q12_pdg=0;   q21_pdg=0; q22_pdg=0;

   return;
}
