#include "TruthDecayContainer/DecayHandle.h"

// System include(s):
#include <memory>
#include <cstdlib>

// ROOT include(s):
#include <TError.h>
#include <TString.h>

// EDM include(s):
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"

// Other include(s):
#include "AsgTools/StatusCode.h"

#include "TruthDecayContainer/ProcClassifier.h"
#include "TruthDecayContainer/TruthDecayUtils.h"

typedef TLorentzVector tlv;

//////////////////////////////////////////////////////////////////////////////////////////////////////
float DecayHandle::PolReweightPerBoson(const float &mchi_heavy, const int &chi_heavy_pdg, 
				       const float &mchi_light, 
				       const Decay_boson &boson){

  //
  // Get the helicity fraction for the boson from the masses of the decay chain
  //
  
  // Default: unpolarized
  float F0=1./3.;
  float FTL=1./3.;
  float FTR=1./3.;

  // C->W+N
  if( isChargino(chi_heavy_pdg) && std::abs(boson.pdg)==24 ){    
    F0  =  m_spinMap_C_W_F0->GetBinContent(  m_spinMap_C_W_F0->FindBin(mchi_heavy,mchi_light));
    FTL = m_spinMap_C_W_FTL->GetBinContent( m_spinMap_C_W_FTL->FindBin(mchi_heavy,mchi_light));
    FTR = m_spinMap_C_W_FTR->GetBinContent( m_spinMap_C_W_FTR->FindBin(mchi_heavy,mchi_light));
  }
  // C->Z+C
  else if( isChargino(chi_heavy_pdg) && boson.pdg==23 ){
    // Not supported yet!
  }

  // N->Z+N
  else if( isNeutralino(chi_heavy_pdg) && boson.pdg==23 ){
    F0  =  m_spinMap_N_Z_F0->GetBinContent(  m_spinMap_N_Z_F0->FindBin(mchi_heavy,mchi_light));
    FTL = m_spinMap_N_Z_FTL->GetBinContent( m_spinMap_N_Z_FTL->FindBin(mchi_heavy,mchi_light));
    FTR = m_spinMap_N_Z_FTR->GetBinContent( m_spinMap_N_Z_FTR->FindBin(mchi_heavy,mchi_light));
  }
  // N->W+C
  else if( isNeutralino(chi_heavy_pdg) && std::abs(boson.pdg)==24 ){
    // Not supported yet!
  }

  //  
  // Calculate weight based on the helicity angle
  //
  
  // Take the fermion (i.e. not anti-fermion)
  TLorentzVector pchild = boson.q1_pdg>0 ? boson.pq1 : boson.pq2;

  // x: helicity angle
  float x = TruthDecayUtils::costh_decay(pchild, boson.P4);
  float weight=1.;

  // W->ff
  if(std::abs(boson.pdg)==24){
    
    weight = F0*(3/4.)*(1-x*x) + FTL*(3/8.)*(1-x)*(1-x) + FTR*(3/8.)*(1+x)*(1+x) ;

  }

  // Z->ff
  else if(boson.pdg==23){

    // Angular distribution for a polarized Z depends on charges of decaying fermions (Q,T3)
    float cVminusA=1., cVplusA=1.;

    // vv
    if(std::abs(boson.q1_pdg)==11 || std::abs(boson.q1_pdg)==13 || std::abs(boson.q1_pdg)==15){
      cVminusA=0.611; cVplusA=0.389;
    }
    // ee/mumu/tautau
    else if(std::abs(boson.q1_pdg)==12 || std::abs(boson.q1_pdg)==14 || std::abs(boson.q1_pdg)==16){
      cVminusA=1.; cVplusA=0.;
    }
    // uu/cc
    else if(std::abs(boson.q1_pdg)==2 || std::abs(boson.q1_pdg)==4){
      cVminusA=0.849; cVplusA=0.151;
    }
    // dd/ss/bb
    else if(std::abs(boson.q1_pdg)==1 || std::abs(boson.q1_pdg)==3 || std::abs(boson.q1_pdg)==5){
      cVminusA=0.971; cVplusA=0.029;
    }
    float w0  = (3/4.)*(1-x*x);
    float wTL = cVplusA *(3/8.)*(1-x)*(1-x) + cVminusA*(3/8.)*(1+x)*(1+x);
    float wTR = cVminusA*(3/8.)*(1-x)*(1-x) + cVplusA *(3/8.)*(1+x)*(1+x);
    weight = F0*w0 + FTL*wTL + FTR*wTR ;
  }
  
  // Return gracefully
  return weight;

}
//////////////////////////////////////////////////////////////////////////////////////////////////////
void DecayHandle::GetPolarizationWeight(float &w1, float &w2){

  TruthEvent_XX *evt = new TruthEvent_XX();
  GetDecayChain_XX(evt); 

  w1 = PolReweightPerBoson(evt->pchi1.M(), evt->chi1_pdg, evt->pN1A.M(), evt->B1);
  w2 = PolReweightPerBoson(evt->pchi2.M(), evt->chi2_pdg, evt->pN1B.M(), evt->B2);

  return ;
  
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
float DecayHandle::GetPolarizationWeight(){
  float w1=1., w2=1.;
  GetPolarizationWeight(w1,w2);
  return w1 * w2;
}
