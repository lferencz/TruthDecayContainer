#include "TruthDecayContainer/Decay_boson.h"

#include <iostream>
#include <vector>

#include "TLorentzVector.h"

#include "TruthDecayContainer/TruthDecayUtils.h"

///////////////////////////////////////////////////////////////
//
// Base class of Decay_W/Z/H. Store two-body decay information
//
//////////////////////////////////////////////////////////////

typedef TLorentzVector tlv;

Decay_boson::Decay_boson() :
  decayLabel(-1),
  pdg(0),
  q1_pdg(0),
  q2_pdg(0)
{ 
  clear(); 
}

Decay_boson::~Decay_boson(){}

////////////////////////////////////////////////////////////////
Decay_boson::Decay_boson(const Decay_boson &rhs):
  TObject(rhs),
  P4(rhs.P4),
  pq1(rhs.pq1),
  pq2(rhs.pq2),
  decayLabel(rhs.decayLabel),
  pdg(rhs.pdg),
  q1_pdg(rhs.q1_pdg),
  q2_pdg(rhs.q2_pdg),
  tau1(rhs.tau1),
  tau2(rhs.tau2),
  pMis(rhs.pMis)
{
}

///////////////////////////////////////////////////
void Decay_boson::finalize(){

  // input : 4-vector, pdg of V
  //         V children's tlv, pdg, tau1, tau2 (if needed)

  if(std::abs(pdg)==24) finalize_W();
  else if(pdg==23)      finalize_Z();
  else{
    // Higgs? skip
  }
  return ;
}
///////////////////////////////////////////////////
void Decay_boson::finalize_W(){
  
  // no tau
  if((abs(q1_pdg)==11 || abs(q1_pdg)==13)){
    decayLabel = 1;
    pMis = pq2;
  }
  else {
    decayLabel = 0;
    pMis.SetXYZM(0,0,0,0);
  }
  
  // with tau. W decaylabel=2 (leptonic tau), =3 (hadronic tau)
  if(std::abs(q1_pdg)==15 && tau1.P4.E()>0){
    
    // decay label (tau_decayLabel=8: leptonic tau decay)
    decayLabel = tau1.decayLabel==8 ? 2 : 3;  
    
    // MET
    pMis = pq2 + (pq1-tau1.pchild);      
  }
      
  check_swap_W();

  return;
}

///////////////////////////////////////////////////
void Decay_boson::finalize_Z(){

  // no tau
  if(std::abs(q1_pdg)!=15 && std::abs(q2_pdg)!=15){
    decayLabel = 
      (abs(q1_pdg)==11 || abs(q1_pdg)==13) ? 1  // ll
      : (abs(q1_pdg)==12 || abs(q1_pdg)==14 || abs(q1_pdg)==16) ? 5 // nunu
      : 0;  // qq
  }  

  // with tau
  else if(std::abs(q1_pdg)==15 && std::abs(q2_pdg)==15){

    if(tau1.decayLabel==8 && tau2.decayLabel==8) // 2leptonic tau decay
      decayLabel=2;    
    else if(tau1.decayLabel!=8 && tau2.decayLabel!=8) // 2hadronic tau decay
      decayLabel=4;
    else  // 1leptonic-1hadronic tau decay
      decayLabel=3;
    
    // MET
    pMis = (pq1-tau1.pchild) + (pq2-tau2.pchild);
  }

  else{
    std::cout << "Decay_Z::finalize  ERROR   Z->1tau evens detected." << std::endl;
    std::cout << "  q1_pdg: " << q1_pdg  
	 << "  q2_pdg: " << q2_pdg
	 << std::endl;
    std::cout << " Exit." << std::endl;
  }

  //
  check_swap_Z();

  return;
}

///////////////////////////////////////////////////
TString Decay_boson::getDecayString(int in_pdg, int in_decayLabel){

  if(std::abs(in_pdg)==24) return getDecayString_W(in_decayLabel);
  else if(in_pdg==23)      return getDecayString_Z(in_decayLabel);
  else{
    // Higgs?
  }
  return "";
}

///////////////////////////////////////////////////
TString Decay_boson::getDecayString_W(int in_decayLabel){

  // decay string
  switch(in_decayLabel) {
  case 0:     return "had";    
  case 1:     return "lep";    
  case 2:     return "taulep";    
  case 3:     return "tauhad";    
  default:    return "unknown";    
  }

  return "";

}
///////////////////////////////////////////////////
TString Decay_boson::getDecayString_Z(int in_decayLabel){
 
  // decay string
  switch(in_decayLabel) {
  case 0:     return "qq";    
  case 1:     return "ll";    
  case 2:     return "#tau_{l}#tau_{l}";    
  case 3:     return "#tau_{l}#tau_{h}";    
  case 4:     return "#tau_{h}#tau_{h}";    
  case 5:     return "#nu#nu";    
  default:    return "unknown";    
  }
  
}


/////////////////////////////////////////
void Decay_boson::swap12(){
  TruthDecayUtils::swap(pq1,pq2);
  TruthDecayUtils::swap(q1_pdg,q2_pdg);
  TruthDecayUtils::swap(tau1,tau2);
}

///////////////////////////////////////////////////
void Decay_boson::check_swap_W(){

  if(decayLabel==0){
    if(q1_pdg>0 && q2_pdg<0) swap12(); // If full hadronic -> always set q1 as one with pdgId>0                                                                                                        
  }
  else{
    if(  (std::abs(q1_pdg)==12 || std::abs(q1_pdg)==14 || std::abs(q1_pdg)==16) && (std::abs(q2_pdg)==11 || std::abs(q2_pdg)==13 || std::abs(q2_pdg)==15) )
      swap12(); // If semi-leptonic -> always set q1 as e/mu/tau                                                                                                                                       
  }
}
///////////////////////////////////////////////////
void Decay_boson::check_swap_Z(){
  if(q1_pdg>0 && q2_pdg<0) swap12(); // always set q1 as one with pdgId>0
}


/////////////////////////////////////////
void Decay_boson::print(){

  std::cout << "  Decay_boson::print() --------------------------------------" << std::endl;
  std::cout << "    pdg: " <<  pdg << std::endl;
  TruthDecayUtils::print4mom(P4, "P4");

  TruthDecayUtils::print4mom(pq1, "pq1");
  TruthDecayUtils::print4mom(pq2,  "pq2");
  std::cout << "    q1_pdg: " <<  q1_pdg << std::endl;
  std::cout << "    q2_pdg: " <<  q2_pdg << std::endl;

  if(tau1.P4.E()>0) tau1.print();
  if(tau2.P4.E()>0) tau2.print();
  std::cout << "  --------------------------------------------------------" << std::endl;
}

/////////////////////////////////////////
void Decay_boson::clear(){

  // tlv
  P4.SetXYZM(0,0,0,0);

  // children
  pq1.SetXYZM(0,0,0,0); pq2.SetXYZM(0,0,0,0);  

  // decaylabel
  decayLabel=-1;

  // pdg
  pdg=0;   q1_pdg=0; q2_pdg=0;

  // tau
  tau1.clear();
  tau2.clear();

  // met
  pMis.SetXYZM(0,0,0,0);
}
/////////////////////////////////////////
