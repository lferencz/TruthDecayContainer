#include "TruthDecayContainer/TruthEvent_VVV.h"

#include <iostream>
#include <vector>

#include "TLorentzVector.h"

#include "TruthDecayContainer/TruthDecayUtils.h"

typedef TLorentzVector tlv;


TruthEvent_VVV::TruthEvent_VVV(){    clear();  }

TruthEvent_VVV::~TruthEvent_VVV(){}

TruthEvent_VVV::TruthEvent_VVV(const TruthEvent_VVV &rhs) :
  TObject(rhs),
  B1(rhs.B1),
  B2(rhs.B2),
  B3(rhs.B3),
  H(rhs.H),
  pMis(rhs.pMis),
  idx_H(rhs.idx_H),
  idx_nonH(rhs.idx_nonH)
{}


/////////////////////////////////////////////////////
void TruthEvent_VVV::set_evt( const Decay_boson &in_B1, const Decay_boson &in_B2, const Decay_boson &in_B3){

  B1 = in_B1;  
  B2 = in_B2;  
  B3 = in_B3;  

  finalize();

}
/////////////////////////////////////////////////////
void TruthEvent_VVV::finalize(){

  // input:  
  //  B1, B2, B3
  pMis = B1.pMis+B2.pMis+B3.pMis;

  Decay_boson pboson[3] = { B1, B2, B3 };  
  int boson_q[3] = { 
    std::abs(B1.pdg)==24 && B1.pdg>0 ? 1 : std::abs(B1.pdg)==24 && B1.pdg<0 ? -1 : 0,
    std::abs(B2.pdg)==24 && B2.pdg>0 ? 1 : std::abs(B2.pdg)==24 && B2.pdg<0 ? -1 : 0,
    std::abs(B3.pdg)==24 && B3.pdg>0 ? 1 : std::abs(B3.pdg)==24 && B3.pdg<0 ? -1 : 0
  };
 
  float minDM=9999.;

  for(int kk=0; kk<3; kk++){
    if(boson_q[kk]+boson_q[(kk+1)%3]!=0) continue;

    TLorentzVector pHcand = pboson[kk].P4+pboson[(kk+1)%3].P4;
    if( fabs(pHcand.M()-125.) < minDM ){
      minDM = pHcand.M()-125.;
      H.clear();      

      H.pdg=25;
      H.P4 = pHcand; 
      H.pq1 = pboson[kk].P4; 
      H.pq2 = pboson[(kk+1)%3].P4; 
      H.q1_pdg = pboson[kk].pdg; 
      H.q2_pdg = pboson[(kk+1)%3].pdg; 

      Decay_boson dummy; dummy.clear();
      if(boson_q[kk]==0 && boson_q[(kk+1)%3]==0)  // ZZ
	H.finalize(dummy, dummy, pboson[kk], pboson[(kk+1)%3]); 
      else //WW
	H.finalize(pboson[kk], pboson[(kk+1)%3], dummy, dummy); 
      
      idx_H = kk*10 + ((kk+1)%3); 
      idx_nonH = (kk+2)%3; 
    }

  }

  return;
}
//////////////////////////////////////////////////
void TruthEvent_VVV::print(){


  std::cout << "TruthEvent_VVV::print() **************************************" << std::endl;
  if(B1.P4.E()>0){ 
    B1.print();
  }
  else
    std::cout << " No boson1 info." << std::endl;

  std::cout << "         ************************************************" << std::endl;
  if(B2.P4.E()>0) { 
    B2.print();
  }
  else
    std::cout << " No boson2 info." << std::endl;

  std::cout << "         ************************************************" << std::endl;
  if(B3.P4.E()>0) { 
    B3.print();
  }
  else
    std::cout << " No boson3 info." << std::endl;
  
  std::cout << "         ************************************************" << std::endl;
  TruthDecayUtils::print4mom(pMis,  "pMis");  
  std::cout << "  idx_H   : " << idx_H << std::endl;
  std::cout << "  idx_nonH: " << idx_nonH << std::endl;
  std::cout << "********************************************************" << std::endl;

  return;
}

//////////////////////////////////////////////
void TruthEvent_VVV::clear(){

  // tlv
  B1.clear();  B2.clear();  B3.clear();  H.clear();
  pMis.SetXYZM(0,0,0,0);

  // labels
  idx_H=-1;
  idx_nonH=-1;

  return;
}
