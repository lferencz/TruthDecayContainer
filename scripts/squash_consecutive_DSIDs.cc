void print(int begin, int end){
  if(begin==end)
    cout << "|| (DSID==" << begin << ")" << endl;
  else
    cout << "|| (DSID>=" << begin << " && DSID<=" << end << ")" << endl;
}

void squash_consecutive_DSIDs(){

  ifstream fin("log");

  bool isFirstLine=true;
  bool justFlush=false;
  int i_begin=-1;
  int i_latest=-1;
  
  while( !fin.eof() ){
    string line; getline(fin,line); TString sline = line;
    if(sline=="") continue;
    
    int num=atoi(line.c_str());

    if( !(num>=100000 && num<999999) ) {
      cout << "File might be empty! Abort" << endl;
      return;
    }

    if(isFirstLine){
      i_begin=num;
      i_latest=num;
      isFirstLine=false;
      continue;
    }

    //cout << num << endl;
    if(num != i_latest+1){
      //cout << "aaa " << i_begin << " " << i_latest << endl;
      print(i_begin, i_latest);
      i_begin = num;
      justFlush=true;
    }
    else
      justFlush=false;

    i_latest = num;
      
  }

  if(!justFlush)
    print(i_begin, i_latest);

}
