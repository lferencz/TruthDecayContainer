//
//  Demonstration of boson polarization correction in the EWKino pair production signals
//

// System include(s):
#include <memory>
#include <cstdlib>
#include <iostream>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TString.h>
#include <TSystem.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TStore.h"
#ifdef ROOTCORE
#include "xAODRootAccess/TEvent.h"
#else
#include "POOLRootAccess/TEvent.h"
#endif // ROOTCORE
#include "PATInterfaces/CorrectionCode.h"
#include "CPAnalysisExamples/errorcheck.h"

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODJet/JetContainer.h"

// Local include(s):
#include "TruthDecayContainer/DecayHandle.h"
#include "TruthDecayContainer/ProcClassifier.h"

// Helper function
float costhStar(const TLorentzVector &child_lab, const TLorentzVector &parent_lab);

/////////////////////////////////////////////////////////
int main( int argc, char* argv[] ) {

  // The application's name:
  const char* APP_NAME = argv[ 0 ];

  // Check if we received a file name:
  if ( argc < 2 ) {
    Error( APP_NAME, "No file name received!" );
    Error( APP_NAME, "  Usage: %s [xAOD file name]", APP_NAME );
    return 1;
  }

  // Open the input file:
  TString fileName;
  fileName = argv[1];
  Info( APP_NAME, "Opening file: %s", fileName.Data() );
  std::auto_ptr< TFile > ifile( TFile::Open( fileName, "READ" ) );
  CHECK( ifile.get() );

  // Create a TEvent object:
#ifdef ROOTCORE
  xAOD::TEvent event( xAOD::TEvent::kAthenaAccess );
#else
  POOL::TEvent event( POOL::TEvent::kAthenaAccess );
#endif

  CHECK( event.readFrom( ifile.get() ) );
  Info( APP_NAME, "Number of events in the file: %i",
        static_cast< int >( event.getEntries() ) );

  xAOD::TStore store;
  
  // Get DSID
  const xAOD::EventInfo* eventInfo = 0;
  event.getEntry(0);
  CHECK( event.retrieve( eventInfo, "EventInfo") );

  if(!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)){
    Error( APP_NAME, "This is a data file. Exit. ");
    return 0;
  }
  
  int dsid = eventInfo->mcChannelNumber();
  Info(APP_NAME,"  DSID: %i ", dsid);

  if(!ProcClassifier::IsXX(dsid)){
    Error(APP_NAME,"This DSID is not found in the XX sample list. Check out TruthDecayContainer/ProcClassfier.h. Exit.");
    return 1;
  }

  // Create the tool(s) to test:
  DecayHandle *decayHandle = new DecayHandle();

  bool convertFromMeV=true; // display everything in GeV
  decayHandle->init(dsid, convertFromMeV);

  // Create container instance
  TruthEvent_XX *evt    = new TruthEvent_XX();

  // ------------------------- Loop over the events ------------------------- //

  int nevt=5; // just first 5 events!
  for ( int entry = 0; entry < nevt; ++entry ) {

    // Tell the object which entry to look at:
    event.getEntry( entry );

    // Retrieve containers
    const xAOD::TruthEventContainer*    truthEvents    = 0;
    const xAOD::TruthParticleContainer* truthParticles = 0;
    const xAOD::TruthParticleContainer* truthElectrons = 0;
    const xAOD::TruthParticleContainer* truthMuons     = 0;
    const xAOD::TruthParticleContainer* truthTaus      = 0;
    const xAOD::TruthParticleContainer* truthNeutrinos = 0;
    const xAOD::JetContainer*           truthJets      = 0;
    CHECK( event.retrieve( truthEvents, "TruthEvents" ) );
    CHECK( event.retrieve( truthParticles, "TruthParticles" )  );
    CHECK( event.retrieve( truthElectrons, "TruthElectrons" )  );
    CHECK( event.retrieve( truthMuons, "TruthMuons" )  );
    CHECK( event.retrieve( truthTaus, "TruthTaus" )  );
    CHECK( event.retrieve( truthJets, "AntiKt4TruthJets" )  );  // Change here to AntiKt4TruthDressedWZJets etc. in case your derivation does not contain AntiKt4TruthJets collection

    decayHandle->loadContainers(truthParticles, truthElectrons, truthMuons, truthTaus, truthNeutrinos, truthJets);

    Info(APP_NAME, "////////////////////// Event: %i //////////////////////////////", entry);

    // Get polarization weight per boson
    float w1=1., w2=1.;
    decayHandle->GetPolarizationWeight(w1, w2); 

    //
    // or simply do 
    // 
    //   float polWeight = decayHandle->GetPolarizationWeight(); 
    // 
    // which is equal to w1*w2
    //

    Info(APP_NAME, "PDGID of the boson from 1st gaugino:  %i", evt->B1.pdg);
    Info(APP_NAME, "Decay label the boson from 1st gaugino:  %i", evt->B1.decayLabel);
    Info(APP_NAME, "Polarization weight for 1st boson:  %f", w1);
    Info(APP_NAME, "PDGID of the boson from 2nd gaugino:  %i", evt->B2.pdg);
    Info(APP_NAME, "Decay label the boson from 2nd gaugino:  %i", evt->B2.decayLabel);
    Info(APP_NAME, "Polarization weight for 2nd boson:  %f", w2);
    Info(APP_NAME, "Event weight:  %f", w1*w2);
    
  }

  return 0;
}
